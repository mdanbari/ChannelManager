package net.fava.web.service;

import net.fava.web.domain.Request;
import net.fava.web.service.dto.RequestDTO;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Request.
 */
public interface RequestService {

    /**
     * Save a request.
     *
     * @param request the entity to save
     * @return the persisted entity
     */
    Request save(Request request);

    /**
     *  Get all the requests.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Request> findAll(Pageable pageable);

    /**
     *  Get the "id" request.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Request findOne(Long id);

    /**
     *  Delete the "id" request.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

	List<Long> fetchRequestCountToSend(Integer mailJobFetchLimit, String string, Integer retryCount);

	String saveRequestDTO(RequestDTO request);
}
