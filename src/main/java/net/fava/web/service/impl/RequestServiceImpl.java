package net.fava.web.service.impl;

import net.fava.web.service.RequestService;
import net.fava.web.service.dto.RequestDTO;
import net.fava.web.domain.Request;
import net.fava.web.repository.RequestRepository;

import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Request.
 */
@Service
@Transactional
public class RequestServiceImpl implements RequestService {

	private final Logger log = LoggerFactory.getLogger(RequestServiceImpl.class);

	private final RequestRepository requestRepository;

	public RequestServiceImpl(RequestRepository requestRepository) {
		this.requestRepository = requestRepository;
	}

	/**
	 * Save a request.
	 *
	 * @param request
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Override
	public Request save(Request request) {
		log.debug("Request to save Request : {}", request);
		return requestRepository.save(request);
	}

	/**
	 * Get all the requests.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<Request> findAll(Pageable pageable) {
		log.debug("Request to get all Requests");
		return requestRepository.findAll(pageable);
	}

	/**
	 * Get one request by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public Request findOne(Long id) {
		log.debug("Request to get Request : {}", id);
		return requestRepository.findOne(id);
	}

	/**
	 * Delete the request by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Request : {}", id);
		requestRepository.delete(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Long> fetchRequestCountToSend(Integer mailJobFetchLimit, String channel, Integer retryCount) {
		return requestRepository.fetchRequestCountToSend(channel, retryCount, new PageRequest(0, mailJobFetchLimit));
	}

	@Override
	public String saveRequestDTO(RequestDTO requestDTO) {
		List<String> receiverList = requestDTO.getReceiver();
		for (String reciever : receiverList) {
			Request request = new Request();
			request.setChannel(requestDTO.getChannel());
			request.setSender(requestDTO.getSender());
			request.setMetadata(requestDTO.getMetadata());
			request.setRetryCount(0);
			request.setRequestCreatedDate(ZonedDateTime.now());
			request.setResult("");
			request.setReceiver(reciever);
			save(request);
		}
		return "SENT_OK";
	}
}
