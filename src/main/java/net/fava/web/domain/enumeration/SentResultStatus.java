package net.fava.web.domain.enumeration;

/**
 * The SentResultStatus enumeration.
 */
public enum SentResultStatus {
    SMS_EXCEPTION, EXECPTION_MIME_MESSAGE, MAIL_EXCEPTION, SENT_OK
}
