package net.fava.web.channel.schedule;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import net.fava.web.channel.util.SmsUtil;
import net.fava.web.service.RequestService;

@Component
@PropertySources(@PropertySource("classpath:application.properties"))
public class ScheduleSmsTask {

	// * "0 0 * * * *" = the top of every hour of every day.
	// * "*/10 * * * * *" = every ten seconds.
	// * "0 0 8-10 * * *" = 8, 9 and 10 o'clock of every day.
	// * "0 0/30 8-10 * * *" = 8:00, 8:30, 9:00, 9:30 and 10 o'clock every day.
	// * "0 0 9-17 * * MON-FRI" = on the hour nine-to-five weekdays
	// * "0 0 0 25 12 ?" = every Christmas Day at midnight

	// * 1 * * * * *

	@Autowired
	SmsUtil smsUtil;
	@Autowired
	private RequestService requestService;

	@Value("${smsJobFetchLimit}")
	private Integer smsJobFetchLimit;

	@Value("${retryCount}")
	private Integer retryCount;

	@Scheduled(fixedDelayString = "${smsfixedDelay}")
	public void runTask() throws IOException, InterruptedException {

		List<Long> smsIdList = requestService.fetchRequestCountToSend(smsJobFetchLimit, "sms",retryCount);
		for (Long smsId : smsIdList) {
			smsUtil.sendSMS(smsId);

		}
	}

	// @Autowired private TaskScheduler taskScheduler;
	//
	// public void scheduleSomething() {
	// Runnable task = new Runnable() {
	// public void run() {
	// List<Long> smsIdList =
	// requestService.fetchRequestCountToSend(smsJobFetchLimit,"sms");
	// for (Long smsId : smsIdList) {
	// smsUtil.sendSMS(smsId);
	// }
	// };
	// taskScheduler.scheduleWithFixedDelay(task, delay);
	// }

}
