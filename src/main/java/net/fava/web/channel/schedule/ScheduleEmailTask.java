package net.fava.web.channel.schedule;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import net.fava.web.channel.util.EmailUtil;
import net.fava.web.service.RequestService;

@Component
public class ScheduleEmailTask {

	@Autowired
	EmailUtil emailUtil;
	@Autowired
	RequestService requestService;
	
	
	@Value("${mailJobFetchLimit}")
	private Integer mailJobFetchLimit;
	
	@Value("${retryCount}")
	private Integer retryCount;

	@Scheduled(fixedDelayString = "${emailfixedDelay}")
	public void runTask() throws  ClientProtocolException, IOException, MessagingException {

		List<Long> emailIdList = requestService.fetchRequestCountToSend(mailJobFetchLimit,"mail",retryCount);
		for (Long emailId : emailIdList) {
			emailUtil.sendEmail(emailId);

		}
	}

}
