package net.fava.web.channel.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import net.fava.web.domain.Request;
import net.fava.web.domain.enumeration.SentResultStatus;
import net.fava.web.repository.RequestRepository;

@Component
public class SmsUtil {

	@Autowired
	RequestRepository requestService;

	@Value("${sms.url}")
	private String url;

	@Value("${sms.username}")
	private String username;

	@Value("${sms.password}")
	private String password;

	@Value("${sms.from}")
	private String from;

	// @Async
	@Transactional
	public void sendSMS(Long smsId) throws  IOException {
		Request requestEntity = requestService.findOne(smsId);
		String mobileNo = requestEntity.getReceiver();
		String metadata = requestEntity.getMetadata();
		requestEntity.setLastRetryDate(ZonedDateTime.now());
		requestEntity.setRetryCount(requestEntity.getRetryCount() == null ? 1 : requestEntity.getRetryCount() + 1);

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);

		List<BasicNameValuePair> urlParameters = new ArrayList<BasicNameValuePair>();
		urlParameters.add(new BasicNameValuePair("username", username));
		urlParameters.add(new BasicNameValuePair("password", password));
		urlParameters.add(new BasicNameValuePair("to", mobileNo));
		urlParameters.add(new BasicNameValuePair("text", metadata));
		urlParameters.add(new BasicNameValuePair("From", from));
		post.setEntity(new UrlEncodedFormEntity(urlParameters, "UTF-8"));

		try {
			HttpResponse response = client.execute(post);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			requestEntity.setResult(result.toString().length() > 4000 ? result.toString().substring(0,4000) : result.toString());
			requestEntity.setSentDate(ZonedDateTime.now());
			requestService.save(requestEntity);
		} catch (IOException e) {
			requestEntity.setSentResultStatus(SentResultStatus.SMS_EXCEPTION);
			requestEntity.setResult(e.getMessage().length() > 4000 ? e.getMessage().substring(0,4000) : e.getMessage().toString());
			requestService.save(requestEntity);

		}

	}

}
