package net.fava.web.channel.util;

import java.time.ZonedDateTime;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import net.fava.web.domain.Request;
import net.fava.web.domain.enumeration.SentResultStatus;
import net.fava.web.repository.RequestRepository;


//Anbari---test
@Component
public class EmailUtil {

	@Autowired
	JavaMailSender javaMailSender;
	@Autowired
	RequestRepository requestRepository;
	@Value("${mail.from}")
	private String from;

	// @Async
	@Transactional
	public void sendEmail(Long emailId){

		Request requestEntity = requestRepository.findOne(emailId);
		requestEntity.setLastRetryDate(ZonedDateTime.now());
		requestEntity.setRetryCount(requestEntity.getRetryCount() == null ? 1 : requestEntity.getRetryCount() + 1);
		requestRepository.save(requestEntity);

		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			
			helper = new MimeMessageHelper(mimeMessage, true);
			helper.setFrom(from);
			helper.setTo(requestEntity.getReceiver());
			helper.setSubject("EMPTY");
			helper.setText(requestEntity.getMetadata(), true);
			
			javaMailSender.send(mimeMessage);
			
			requestEntity.setSentDate(ZonedDateTime.now());
			requestEntity.setSentResultStatus(SentResultStatus.SENT_OK);
			requestRepository.save(requestEntity);
			
		} catch (MessagingException e) {
			
			requestEntity.setResult(e.getMessage().length() > 4000 ? e.getMessage().substring(0,4000) : e.getMessage().toString());
			requestEntity.setSentResultStatus(SentResultStatus.EXECPTION_MIME_MESSAGE);
			requestRepository.save(requestEntity);
			
		} catch (MailException e) {
			
			requestEntity.setResult(e.getMessage().length() > 4000 ? e.getMessage().substring(0,4000) : e.getMessage().toString());
			requestEntity.setSentResultStatus(SentResultStatus.MAIL_EXCEPTION);
			requestRepository.save(requestEntity);
		}

	}

}
