package net.fava.web.repository;

import net.fava.web.domain.Request;
import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;


/**
 * Spring Data JPA repository for the Request entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RequestRepository extends JpaRepository<Request,Long> {

	@Query("select id from Request where sentDate is null and channel = :channel and retryCount <= :retryCount")
	List<Long> fetchRequestCountToSend(@Param("channel") String channel, @Param("retryCount") Integer retryCount , Pageable fetchLimit);
    
}
