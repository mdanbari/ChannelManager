(function() {
    'use strict';

    angular
        .module('channelmanagerApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
