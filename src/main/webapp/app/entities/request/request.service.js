(function() {
    'use strict';
    angular
        .module('channelmanagerApp')
        .factory('Request', Request);

    Request.$inject = ['$resource', 'DateUtils'];

    function Request ($resource, DateUtils) {
        var resourceUrl =  'api/requests/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.requestCreatedDate = DateUtils.convertDateTimeFromServer(data.requestCreatedDate);
                        data.lastRetryDate = DateUtils.convertDateTimeFromServer(data.lastRetryDate);
                        data.sentDate = DateUtils.convertDateTimeFromServer(data.sentDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
